#include <stdio.h>
#include <stdlib.h>

typedef struct arvore
{
    char info;
    struct arvore *esq;
    struct arvore *dir;
} Arvore;


Arvore*  cria_arv_vazia (void);
Arvore*  arv_constroi (int c, Arvore* e, Arvore* d);
int      verifica_arv_vazia (Arvore* a);
Arvore*  arv_libera (Arvore* a);
int      arv_pertence (Arvore* a, char c);
void     arv_imprime (Arvore* a);

Arvore* cria_arv_vazia (void)
{
    return NULL;
}

Arvore* arv_constroi (int c, Arvore* e, Arvore* d)
{
    Arvore* a = (Arvore*)malloc(sizeof(Arvore));
    a->info = c;
    a->esq = e;
    a->dir = d;
    return a;
}

int verifica_arv_vazia (Arvore* a)
{
    return (a == NULL);
}

Arvore* arv_libera (Arvore* a)
{
    if (!verifica_arv_vazia(a))
    {
        arv_libera (a->esq);
        arv_libera (a->dir);
        free(a);
    }
    return NULL;
}

int arv_bin_check(Arvore * a){
//precisa verificar se o n� direito � maior que a raiz
//precisa verificar se o n� esquerdo � menor que a raiz
//caso existir uma dessas inconsistencias, n�o � um arvore binaria de busca
    Arvore *aux;
    if (a == NULL) {
            printf("\n !!! essa eh uma arvore binaria de busca");
            return 1;
    }
    if (a->dir && a->esq){
        if (a->dir <= a->esq)
            return 0;
    }
    if (a->esq) {
            aux = a->esq;
            if (aux ->info > a->info){
                printf("\n nao eh uma arvore binaria de busca pois %d eh maior que %d e esta a sua esquerda", aux->info, a->info);
                return 0;
            }
                else{
                        return arv_bin_check(a->esq);
            }
            }
    else if (a->dir) {
            aux = a->dir;
            if (aux ->info < a->info){
                printf("\n nao eh uma arvore binaria de busca pois %d eh menor que %d e esta a sua direita", aux->info, a->info);
                return 0;
            }
                else{
                        return arv_bin_check(a->dir);
            }
            }
    else { return 1; }
}

int main (int argc, char *argv[])
{
    Arvore *a, *a1, *a2, *a3, *a4, *a5, *a6;
    a1 = arv_constroi(4,cria_arv_vazia(),cria_arv_vazia());
    a6 = arv_constroi(20,cria_arv_vazia(),cria_arv_vazia());
    a2 = arv_constroi(7,cria_arv_vazia(),a1);
    a3 = arv_constroi(15,cria_arv_vazia(),cria_arv_vazia());
    a4 = arv_constroi(60,cria_arv_vazia(),a6);
    a5 = arv_constroi(10,a3,a4);
    a  = arv_constroi(9,a2,a5);


  printf("\n");
  printf("\n \n \n \n");
  if(arv_bin_check(a))
    printf("\n eh uma arvore binaria de busca");
  else
    printf("\n nao eh uma arvore binaria de busca");

  printf("\n");
  return 0;
}

